import { fabric } from "fabric";

/**
 * GroupExtended class
 * @extends fabric.Group
 * @todo object stacking
 * @todo group inside a group
 * @todo group events
 */
export const GroupExtended = fabric.util.createClass(fabric.Group, {
  /**
   * Type of an object
   * @type String
   */
  type: "group-extended",

  /**
   * Indicates if click, mouseover, mouseout events & hoverCursor should also check for subtargets
   * @type Boolean
   */
  subTargetCheck: true,

  /**
   * When set to `false`, object's controlling borders are not rendered
   * @type Boolean
   */
  hasBorders: true,

  /**
   * Array specifying dash pattern of an object's borders (hasBorder must be true)
   * @type Array
   */
  borderDashArray: [5, 5],

  /**
   * Opacity of inactive objects when a known target object is selected
   * @type number
   */
  inactiveObjectOpacity: 0.4,

  /**
   * Last known selected object
   * @type fabric.Object
   */
  _lastSelectedObject: null,

  /**
   * Called when target object or grup is selected
   */
  _onTargetSelected: function (selected) {
    const objects = this._objects;

    for (let i = 0, len = objects.length; i < len; i++) {
      const object = objects[i];

      if (object === selected) {
        object.set("opacity", 1);
      } else if (selected.type === this.type) {
        object.set("opacity", 1);
      } else {
        object.set("opacity", this.inactiveObjectOpacity);
      }
    }

    this._lastSelectedObject = selected;
    this.canvas.requestRenderAll();
  },

  /**
   * Called when group is deselected
   */
  _onTargetDeselected: function () {
    const objects = this._objects;

    for (let i = 0, len = objects.length; i < len; i++) {
      objects[i].set("opacity", 1);
    }
    this._lastSelectedObject = null;
  },

  /**
   * Constructor
   * @param {Object} objects Group objects
   * @param {Object} [options] Options object
   * @param {Boolean} [isAlreadyGrouped] if true, objects have been grouped already
   * @return {Object} thisArg
   */
  initialize: function (objects, options, isAlreadyGrouped) {
    this.on("mousedown", this._findTarget);
    this.on("deselected", this._onTargetDeselected);

    this.callSuper("initialize", objects, options, isAlreadyGrouped);
  },

  /**
   * Find target with subtarget check
   * @param {Object} opts mouse event
   */
  _findTarget: function ({ e, target, subTargets }) {
    const selected = (subTargets && subTargets[0]) || target;
    const lastSelected = this._lastSelectedObject;

    if (selected && selected !== lastSelected) {
      this._onTargetSelected(selected);
      this.canvas.fire("group:selection:created", { e, target: selected });
    }
  },
});
