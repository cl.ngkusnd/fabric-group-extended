# GroupExtended

Extend the functionality of the Fabric.js Group.

### Getting started

GroupExtended is inherited from the [Group](http://fabricjs.com/docs/fabric.Group.html) class. Group methods and events are still available on GroupExtended.

### Features

- Select object inside Group
- Special effects for selected object and inactive objects

### To-do List

- Object stacking
- Group inside a group
- Group events

### Options

| name                    | type  | default | description                                                        |
| ----------------------- | ----- | ------- | ------------------------------------------------------------------ |
| `inactiveObjectOpacity` | float | 0.4     | Opacity of inactive objects when a known target object is selected |

### Canvas Events

| name                      | params    | description                                            |
| ------------------------- | --------- | ------------------------------------------------------ |
| `group:selection:created` | e, target | Fires when target object or grup is focused (selected) |

### Example

    import { GroupExtended } from "fabric-group-extended";

    ...
    const groupExtended = new GroupExtended(objects, {
        inactiveObjectOpacity: 0.8
    });
    ...
    canvas.on("group:selection:created", ({e, target}) => {});

### License
